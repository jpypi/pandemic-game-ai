from copy import deepcopy

from player import Player
from city import City, import_cities
from cards import PlayerCard, PlayerCardDeck, ShuffleDeck
from actions import Share, Clean, Cure, Build, DiscardMove, FreeMove


class Game:
    def __init__(self, n_players, difficulty, AI_Decider):
        self.cities = []

        self.roles = []
        self.player_cards = []
        self.player_discard = []
        self.infection_cards = []
        self.infection_discard = []
        # a subset of infection cards that track which cards are back on top
        # that were in the discard
        self.infection_cards_restack = []

        self.round_number = 0
        self.turn_number = 0

        self.cured_diseases = {
            "blue": False,
            "yellow": False,
            "black": False,
            "red": False
        }

        self.game_lost = False
        self.game_win = False
        self.occurred_epidemics = 0
        self.infection_rate_options = [2, 2, 2, 3, 3, 4, 4]
        self.outbreak_number = 0

        self.AI = AI_Decider

        self.starting_num_of_player_cards = 48 + 5

        # Players
        self.number_of_players = n_players
        self.players = []

        # Set difficulty
        if difficulty == 'easy':
            self.num_of_epidemics = 4
        elif difficulty == 'standard':
            self.num_of_epidemics = 5
        else:
            self.num_of_epidemics = 6

        # need to grab the correct reference
        self.cities = import_cities("pandemic_cities.csv")

        self.generate_card_decks()
        self.spawn_infection()
        self.spawn_characters()
        self.finalize()

    @property
    def infection_rate(self):
        return self.infection_rate_options[self.occurred_epidemics]

    @property
    def current_player(self):
        pi = self.turn_number % self.number_of_players
        return self.players[pi]

    def play(self):
        # Main game loop
        while self.check_win() is None:
            self.update()

        if self.check_win():
            print("DR. FAUCI HAS WON! on turn " + str(self.turn_number))
        else:
            print("CORONAVIRUS HAS WON! on turn " + str(self.turn_number))

    def update(self):
        # TODO: Implement a single turn
        # Run the AI to make decisions
        if self.AI is not None:
            game_copy = deepcopy(self)
            actions = self.AI.SetActionOptions(self.current_player, game_copy)

        self.do_action(actions, self.current_player)
        self.draw_playercards(self.current_player)
        self.draw_infections()
        self.current_player.reset_action_counter()
        self.turn_number += 1

        self.check_for_outbreaks()

    def do_action(self, list_of_actions, player):
        #list of actions (order dependent) for player
        for act in list_of_actions:
            if act.check(player):
                act.perform(player, self)

    def discard_playercard(self, player, index):
        self.player_discard.append(player.card_list.pop(index))

    def draw_playercards(self, player):
        #draw 2 cards
        if self.player_cards.number_of_cards_left > 2:
            #card 1
            c = self.player_cards.DrawCard()
            if c.kind == 'Epidemic':
                self.spawn_epidemic()
            else:
                player.AddCard(c)
            #card 2
            c = self.player_cards.DrawCard()
            if c.kind == 'Epidemic':
                self.spawn_epidemic()
            else:
                player.AddCard(c)
        else:
            self.game_lost = True
            return

    def draw_infections(self):
        for k in range(self.infection_rate):
            infect = self.infection_cards.pop(0)
            infect.has_been_drawn = True
            self.cities[infect.ID].AddDrawnInfection(self.cities, 1, self.players)
            self.infection_discard.append(infect)
            #pull out restack cards if they exist
            if len(self.infection_cards_restack) > 0:
                self.infection_cards.restack.pop(0)

    def spawn_epidemic(self):
        # Part 1: upgrade infection rate
        self.occurred_epidemics += 1

        # Part 2: Cause Infection
        bottom_card = self.infection_cards.pop(-1)
        bottom_card.has_been_drawn = True
        self.cities[bottom_card.ID].AddDrawnInfection(self.cities, 3, self.players)
        self.infection_discard.append(bottom_card)

        # Part 3: reshuffle and place on top
        ShuffleDeck(self.infection_discard)

        # Update local tracker of which cards have been drawn and are back on top
        self.infection_cards_restack.extend(self.infection_discard)
        self.infection_cards = self.infection_discard.extend(self.infection_cards)

    def generate_card_decks(self):
        #role deck
        role_deck = ['Medic',
                     'Researcher',
                     'Operations Expert',
                     'Dispatcher',
                     'Quarantine Specialist',
                     'Scientist',
                     'Contingency Planner']
        ShuffleDeck(role_deck)
        self.roles = role_deck
        #player cards
        city_cards = []
        for city in self.cities:
            city_cards.append(PlayerCard('city', city.name, city.ID))

        #shuffles internally
        self.player_cards = PlayerCardDeck(city_cards)

        # infection cards
        self.infection_cards = city_cards
        ShuffleDeck(self.infection_cards)

    def spawn_infection(self):
        for k in range(3):
            for i in range(3):
                self.cities[self.infection_cards[0].ID].AddDrawnInfection(self.cities,3-k,self.players)
                cardref = self.infection_cards.pop(0)
                cardref.has_been_drawn = True
                self.infection_discard.append(cardref)

    def spawn_characters(self):
        random_names = ["Kevin", "Phil", "Megan", "Jessica"];
        for k in range(self.number_of_players):
            self.players.append(Player(self.roles[k], random_names[k], k))

        #Draw cards for the players
        start_cards = self.player_cards.DrawPlayerStartingCards(self.number_of_players)
        for c, p in enumerate(self.players):
            for k in range(len(start_cards[c])):
                p.AddCard(start_cards[c][k])

    def finalize(self):
        self.player_cards.AddEpidemicCards(self.num_of_epidemics)
        self.cities[0].research_center = True
        #this number tracks remaining cards disregarding turn 0 draw
        self.starting_num_of_player_cards = self.player_cards.remaining_cards()

    def check_for_outbreaks(self):
        total = 0
        for city in self.cities:
            if city.outbreak:
                total += 1
                city.outbreak = False
        self.outbreak_number += total

    def check_cube_limit(self):
        """
        return: True if all of the disease cubes for any color have been placed
        """

        totals = {}
        for city in self.cities:
            for color in self.cured_diseases.keys():
                totals[color] = totals.get(color, 0) + city.disease[color]

        if any(map(lambda v: v > 24, totals.values())):
            return True
        else:
            return False

    def check_win(self):
        """
        A win occurs if all four diseases are cured.

        A loss occurs when any of the following are true:
            - 8 outbreaks
            - more than 24 disease cubes of one color
            - no cards left

        Returns None if game has neither been won nor lost.
        """
        if self.outbreak_number >= 8 or self.check_cube_limit():
            return False

        # No cards left is checked when drawing 2 new cards for player

        # All four diseases cured
        if all(self.cured_diseases.values()):
            return True

        return None

    def share_card(self, src_player, dst_player, card_index):
        dst_player.card_list.append(src_player.card_list.pop(card_index))

    def print_game_state(self):
        print("================")
        print("Diseased Cities:")
        for c in self.cities:
            print(c.StrDiseaseStatus())

        print("================")
        print("Research Centers:")
        for c in self.cities:
            if c.research_center:
                print(f"City: {c.name}({c.ID})")

        print("================")
        print("Players:")
        for p in self.players:
            p.ShowCharacter(self.cities)
            p.ShowActionOptions(self.cities,self.players,self.player_discard)

        print("================")

