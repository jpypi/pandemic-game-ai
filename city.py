import csv


__all__ = ["City", "import_cities"]


class City:
    def __init__(self, ID: int, name: str, color: str, neighbors: list[int]):
        self.ID = ID
        self.name = name
        self.color = color
        self.diseases = {
            "blue": 0,
            "yellow": 0,
            "black": 0,
            "red": 0
        }
        self.neighbors = neighbors

        self.research_center = False
        self.outbreak = False

    def StrDiseaseStatus(self):
        lines = []
        if sum(self.diseases.values()) > 0:
            lines.apend(f"City: {self.name}({self.ID})")
            for color, value in self.diseases.items():
                if value > 0:
                lines.append(f"{color}: {value}")

        return "\n".join(lines)

    def IsQuarentined(self, players):
        for p in players:
            if p.role == "Quarantine Specialist":
                if self.ID == p.position_id:
                    return True
                else:
                    for n in self.neighbors:
                        if n == p.position_id:
                            return True
                return False

        return False

    def AddDrawnInfection(self, cities: list[City], value: int,
                          player_list: list[Player]):
        if self.IsQuarentined(player_list):
            return

        self.diseases[self.color] += value
        self.CheckOutbreak(cities, player_list)

    def AddInfection(self, color: str, cities: list[City],
                     player_list: list[Player]):
        if self.IsQuarentined(player_list):
            return

        self.diseases[color] += 1
        self.CheckOutbreak(cities, player_list)

    def CheckOutbreak(self, cities: list[City], player_list: list[Player]):
        if self.outbreak or self.IsQuarentined(player_list):
            return

        for color, value in self.diseases.items():
            if value > 3:
                self.diseases[color] = 3
                self.GenerateOutbreak(color, cities)

    def GenerateOutbreak(self, color: str, cities: list[City],
                         player_list: list[Player]):
        self.outbreak = True
        for n in self.neighbors:
            cities[n].AddInfection(self, color, cities, player_list)

    def GetNeighbors(self):
        return self.neighbors

    def ClearDisease(self, amount: int, color: str):
        self.diseases[color] = max(0, self.diseases[color] - amount)


def import_cities(filename: str):
    cities = []
    with open(filename) as csvfile:
        cityreader = csv.reader(csvfile)
        # TODO: wrap in try/catch
        for row in cityreader:
            neighbors = [int(n) for n in row[3:]]
            ID, name, color = int(row[0]), row[1], row[2]
            cities.append(City(ID, name, color, neighbors))

    return cities
